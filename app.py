import json
from flask import Flask, request
from flask_restful import Resource, Api
from pymongo import MongoClient
from flask_cors import CORS

app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"/api": {"origins": "*"}})

client = MongoClient('localhost', 27017)
db = client.webowe

class SurveyApi(Resource):
    def get(self):
        with open('ankieta.json', encoding='utf-8') as f:
            data = json.load(f)
        return data

    def put(self):
        template_data = request.get_json()
        insertReturnResponse = db.data.insert_one(template_data)
        return {'success': insertReturnResponse.acknowledged}

class GetAllData(Resource):
    def get(self):
        return list(db.data.find(None, {'_id':0}))

api.add_resource(SurveyApi, '/api')
#api.add_resource(GetAllData, '/data')

if __name__ == '__main__':
    app.run(debug=True)
